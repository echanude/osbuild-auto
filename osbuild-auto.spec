Name:           osbuild-auto
Version:        0.1.5
Release:        1%{?dist}
Summary:        Automotive stages for osbuild

License:        GPLv2+
Source1:	org.osbuild-auto.ostree.deploy-verity
Source2:	org.osbuild-auto.ostree.config
Source3:	org.osbuild-auto.ostree.genkey
Source4:	org.osbuild-auto.ostree.sign
Source5:	org.osbuild-auto.aboot.conf
Source6:	org.osbuild-auto.aboot.update
Source7:	org.osbuild-auto.write-device
Source8:        org.osbuild-auto.ostree.config-compliance-mode

Requires:       osbuild
BuildArch:      noarch

%description
Automotive stages for osbuild

%prep
rm -rf %{name}-{%version}
mkdir %{name}-{%version}

%build
cd %{name}-{%version}

%install
cd %{name}-{%version}
mkdir -p %{buildroot}%{_prefix}/lib/osbuild/stages
install -m755 %{SOURCE1} %{buildroot}%{_prefix}/lib/osbuild/stages/org.osbuild-auto.ostree.deploy-verity
install -m755 %{SOURCE2} %{buildroot}%{_prefix}/lib/osbuild/stages/org.osbuild-auto.ostree.config
install -m755 %{SOURCE3} %{buildroot}%{_prefix}/lib/osbuild/stages/org.osbuild-auto.ostree.genkey
install -m755 %{SOURCE4} %{buildroot}%{_prefix}/lib/osbuild/stages/org.osbuild-auto.ostree.sign
install -m755 %{SOURCE5} %{buildroot}%{_prefix}/lib/osbuild/stages/org.osbuild-auto.aboot.conf
install -m755 %{SOURCE6} %{buildroot}%{_prefix}/lib/osbuild/stages/org.osbuild-auto.aboot.update
install -m755 %{SOURCE7} %{buildroot}%{_prefix}/lib/osbuild/stages/org.osbuild-auto.write-device
install -m755 %{SOURCE8} %{buildroot}%{_prefix}/lib/osbuild/stages/org.osbuild-auto.ostree.config-compliance-mode

%files
%{_prefix}/lib/osbuild/stages/*

%changelog
* Wed Aug 16 2023 Alexander Larsson <alexl@redhat.com> - 0.1.5-1
- Update ostree stages to use base64 keys

* Tue Jul 18 2023 Eric Curtin <ecurtin@redhat.com> - 0.1.4-1
- Ensure writes are sync'd to disk

* Wed Jul 12 2023 Alexander Larsson <alexl@redhat.com> - 0.1.3-1
- Update ostree stages for userspace signature approach

* Thu Jun  1 2023 Alexander Larsson <alexl@redhat.com> - 0.1.1-1
- Update composefs.deploy stage for lazy signatures

* Wed May 31 2023 Alexander Larsson <alexl@redhat.com> - 0.1-2
- Fix description

* Wed May 24 2023 Alexander Larsson <alexl@redhat.com> - 0.1-1
- Initial version
